<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    /*
    Route::get('/homes', function () {
        return response('Error', 404)
                      ->header('Content-Type', 'text/plain');
    });*/
    Route::get('community', [App\Http\Controllers\CommunityLinkController::class, 'index']);
    Route::post('community', [App\Http\Controllers\CommunityLinkController::class, 'store']);

    Route::get('community/{channel}', [App\Http\Controllers\CommunityLinkController::class, 'index']);

    Route::post('/votes/{link}', [App\Http\Controllers\CommunityLinkUserController::class, 'store']);

    Route::get('/upload_image', [App\Http\Controllers\UserAvatarController::class, 'index']);
    Route::post('/upload_image', [App\Http\Controllers\UserAvatarController::class, 'update']);
});


