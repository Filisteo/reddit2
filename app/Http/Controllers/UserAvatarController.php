<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserAvatarController extends Controller
{
    /**
     * Update the avatar for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('imgForm');
    }

    public function update(Request $request)
    {
        $request->validate([
            'image' => 'required|mimes:png,jpg|max:2048',
        ]);

        $request->file('image')->store('public');

        Auth::user()->image = asset('storage/' . $request->file('image')->hashName());
        Auth::user()->save();
    

        return  redirect()->route('home');
    }
}
