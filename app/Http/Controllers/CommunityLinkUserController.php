<?php

namespace App\Http\Controllers;

use App\Models\CommunityLink;
use App\Models\CommunityLinkUser;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommunityLinkUserController extends Controller
{

    public function store(CommunityLink $link)
    {
        // $vote = CommunityLinkUser::firstOrNew(['user_id' => Auth::id(), 'community_link_id' => $link->id]);
        // //vote es una variable que almacena una query, la cual consiste en localizar el modelo que coincida
        // //con los parametros que se le pasen (Que el usuario este autenticado, y que los enlaces coincidan).
        // //Si no no coincide ninguna busqueda, crea un nuevo registro.
        // if ($vote->id) {
        //     //Si coincide, elimina la votación
        //     $vote->delete();
        // } else {
        //     //Si no, guardala.
        //     $vote->save();
        // }
        // return back();

        CommunityLinkUser::firstOrNew([
             'user_id' => Auth::id(),
             'community_link_id' => $link->id
         ])->toggle();

         return back();
    }
}
