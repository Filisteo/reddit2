<?php

namespace App\Http\Controllers;

use App\Models\CommunityLink;
use App\Models\Channel;
use App\Queries\CommunityLinksQuery;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommunityLinkForm;



class CommunityLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Channel $channel = null)
    {
        // $links = CommunityLink::where('approved', true)->latest('updated_at')->paginate(25);
        
        $channels = Channel::orderBy('title', 'asc')->get();
        
        //$channels = Channel::orderBy('slug')->get();
        // return view('community/index', compact('links', 'channels'));

        $communityLinkQuery = new CommunityLinksQuery();

        if($channel != null){
            $links = $communityLinkQuery->getByChannel($channel);
        }else{
            $links = $communityLinkQuery->getAll();
        }

        if (request()->exists('popular')) {
            $links = $communityLinkQuery->getMostPopular();
        }

        if(request()->exists('search')){
            $result = request()->get('search');
            $search = trim($result);
            
            if(strpos($search, ' ')){
               $convertSearch = explode(" ", $search);
               //print_r($convertSearch);
               $links = $communityLinkQuery->getTheLink($search);
            }

            $links = $communityLinkQuery->getTheLink($search);
        }

        return view('community/index', compact('links', 'channels', 'channel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommunityLinkForm $request)
    {
        //dd($request)
        //dd($request->path());
        //dd($request->url());
        //dd($request->input());
        //dd($request->fullUrl());
        
        $approved = Auth::user()->isTrusted() ? true : false;
        
        $request->merge(['user_id' => Auth::id(), 'approved' => $approved]);

        //INSTANCIANDO OBJETO COMMUNITYLINK

        $link = new CommunityLink();
        $link->user_id = Auth::id();

        if ($approved == false) {
           
            if($link->hasAlreadyBeenSubmitted($request->link)){
                return back()->with('success', 'Enlace duplicado');
            }else{
                $link->create($request->all());

                return back()->with('success', 'El link se compartira con exito');
            }    

        } else {
            if($link->hasAlreadyBeenSubmitted($request->link)){
                return back()->with('success', 'Enlace duplicado');
            }else{
                $link->create($request->all());
                return back()->with('success', 'El link se a compartido con exito!');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function show(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function edit(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommunityLink $communityLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommunityLink $communityLink)
    {
        //
    }
}
