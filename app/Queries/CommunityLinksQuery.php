<?php

namespace App\Queries;

use App\Models\CommunityLink;
use App\Models\Channel;

class CommunityLinksQuery
{
    public function getByChannel(Channel $channel)
    {
        return $channel->communitylinks()->where('approved', true)->where('channel_id', $channel->id)->latest('updated_at')->paginate(25);
    }

    public function getAll()
    {
        return CommunityLink::where('approved', true)->latest('updated_at')->paginate(25);
    }

    public function getMostPopular()
    {
        return  CommunityLink::where('approved', true)->withCount('users')->orderBy('users_count', 'desc')->paginate(25);
    }

    public function getTheLink($search){
        return CommunityLink::where('title', 'like', '%'.$search.'%')->orWhere('link', 'like', '%'.$search.'%')->withCount('users')->orderBy('users_count', 'desc')->paginate(25);
    }
}

?>
