
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <?php if($channel): ?>
            <h1><a href="/community">Community</a>-<?php echo e($channel->title); ?></h1>
            <?php else: ?>
            <h1><a href="/community">Community</a></h1>
            <?php endif; ?>

            <?php echo $__env->make('partials.links', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        </div>
        <?php echo $__env->make('partials.add-link', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <?php echo e($links->appends($_GET)->links()); ?>


</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/project1/comunity/resources/views/community/index.blade.php ENDPATH**/ ?>