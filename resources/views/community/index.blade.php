@extends('layouts.app')
@section('content')
@include('flash-message')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            @if($channel)
            <h1><a href="/community">Community</a>-{{$channel->title}}</h1>
            @else
            <h1><a href="/community">Community</a></h1>
            @endif

            @include('partials.links')

        </div>
        @include('partials.add-link')
    </div>
    {{ $links->appends($_GET)->links() }}

</div>
@stop