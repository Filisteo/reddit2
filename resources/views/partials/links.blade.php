@if (count($links) == 0)
<div class="alert alert-warning" role="alert">
    "No contributions yet".
</div>
@endif
<ul class="nav">
    <li class="nav-item">
        <a class="nav-link {{request()->exists('popular') ? '' : 'disabled' }}" href="{{request()->url()}}">Most recent</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{request()->exists('popular') ? 'disabled' : '' }}" href="?popular">Most popular</a>
    </li>
</ul>

@foreach ($links as $link)

<li>
    <form method="POST" action="/votes/{{ $link->id }}">
        {{ csrf_field() }}

        <button type="submit" class="btn {{ Auth::check() && Auth::user()->votedFor($link) ? 'btn-success' : 'btn-secondary' }} mb-2" {{ Auth::guest() ? 'disabled' : '' }}>
            {{$link->users()->count()}}
        </button>
        
        <i class="fab fa-gratipay fa-2x mt-1 mx-1"></i>

        <a href="{{$link->link}}" target="_blank">
            {{$link->title}}
        </a>

        <span class="label label-default mx-1" style="background:{{ $link->channel->color }}">
            <a class=" text-white p-2 text-decoration-none" href="/community/{{ $link->channel->slug }}">
                {{ $link->channel->title }}
            </a>
        </span>

        <small>Contributed by: {{$link->creator->name}} {{$link->updated_at->diffForHumans()}}</small>
    </form>
</li>
@endforeach